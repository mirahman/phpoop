<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

interface Car {
    public function showName();
    public function defineMilage();
}

interface Sportscar {
    public function gearOption();
    public function showTopLimit();
}

Class BMWX7 implements Car, Sportscar {
    
    public function showName() {
	echo "I am showing cost";
    }
    
    public function defineMilage() {
	echo "20M/L";
    }
    
    public function gearOption() {
	echo "Gear # 6";
    }
    public function showTopLimit(){
	echo "380KM/hour";
    }
    
}