<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class RectangleSystem{
    
    public $width;
    public $height;
    
    public function __construct($width, $height) {
	$this->width = $width;
	$this->height = $height;
    }
    
    public function area() {
	return $this->height * $this->width;
    }
    
}