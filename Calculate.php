<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Calculator {
    
   function add(int $a, int $b) :int
   {
       return $a+$b;
   }
   
   function subtract(int $a, int $b) :int
   {
       return $a-$b;
   }
   
   function multiply(int $a, int $b) :int
   {
       return $a*$b;
   }
   
   function divide(int $a, int $b) :float
   {
       return $a/$b;
   }
}

$myCalculator = new Calculator;

echo $myCalculator->multiply(10,30);
