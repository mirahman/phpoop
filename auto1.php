<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function myautoload($className) {
    echo  "I am looking for ".$className.'<br />';
    
    if(is_file("classes/".$className.".php"))
	include "classes/".$className.".php";
}

function systemautoload($className) {
    echo  "I am looking in system folder for ".$className.'<br />';
    if(is_file("system/".$className.".php"))
    include "system/".$className.".php";
}

spl_autoload_register('myautoload');
spl_autoload_register('systemautoload');

//include "classes/Circle.php";
//include "classes/Square.php";
//include "classes/Rectangle.php";

$arr = [];
$arr[] = new Circle(10);
$arr[] = new Square(12);
$arr[] = new Rectangle(20,15);
$arr[] = new RectangleSystem(10,15);

foreach($arr as $obj)
    echo $obj->area()."<br/>";
