<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

abstract class transport {
    public abstract function showName();
}
	
Class Car extends transport{
    
    public function showName() {
	echo "i am car";
    }
    
}

Class Truck extends transport{
    
    public function showName() {
	echo "i am truck";
    }
    
}

Class Bus extends transport{
    
    public function showName() {
	echo "i am bus";
    }
    
}

$bus = new Bus;
$car = new Car;
$truck = new Truck;

$arr = [$car, $bus, $truck];

foreach($arr as $obj)
    echo $obj->showName ()."<br/>";