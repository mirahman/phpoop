<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class AstonMartin {
    
    public $name;
    public $year;
    
    public function setName($name) {
	$this->name = $name;
	return $this;
    }
    
    public function setYear($year) {
	$this->year = $year;
	return $this;
    }
    
    public function showName() {
	echo $this->name;
	return $this;
    }
    
    public function showYear() {
	echo $this->year;
	return $this;
    }
    
}

$am = new AstonMartin;
$am->setName("Aston marting")
    ->setYear(2017)
    ->showName()
    ->showYear();
