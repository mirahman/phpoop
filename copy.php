<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

echo "<pre>";

interface geometry {
    public function area();
}
	
Class Rectangle implements geometry{
    
    public $width;
    public $height;
    
    public function __construct($width, $height) {
	$this->width = $width;
	$this->height = $height;
    }
    
    public function area() {
	return $this->height * $this->width;
    }
    
}

Class Sqaure implements geometry{
    
    public $side;
    
    public function __construct($side) {
	$this->side = $side;
    }
    
    public function area() {
	return $this->side * $this->side;
    }
    
}

Class Circle implements geometry{
    
    public $radius;
    private $diameter;
    
    public function __construct($radius) {
	$this->radius = $radius;
	$this->diameter = 2 * $radius;
    }
    
    public function area() {
	return (22/7)*$this->radius * $this->radius;
    }
    
}

Class Calculate {
    
    public static function getArea(geometry $obj) {
	return $obj->area();
    }
    
}

$arr = [];
$circ1 = new Circle(10);
$circ2 = &$circ1;

var_dump($circ1);
var_dump($circ2);

$circ1->radius = 20;

var_dump($circ1);
var_dump($circ2);

$str = serialize($circ2);
echo serialize([1,2]);
$circ3 = unserialize($str);


foreach($circ3 as $prop => $value) {
    echo $prop." > ".$value."<br />";
}

var_dump($circ3);

$circ1 = NULL;

var_dump($circ1);
var_dump($circ2);