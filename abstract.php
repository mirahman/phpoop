<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

abstract  Class Car {
    
    public function showName() {
	echo "I am here";
    }
    
    abstract  function defineMilage();
    
}

abstract Class BMW extends Car {
    
    abstract function showCost();
}

Class BMWX7 extends BMW{
    
    public function showCost() {
	echo "I am showing cost";
    }
    
    public function defineMilage() {
	echo "20M/L";
    }
    
}