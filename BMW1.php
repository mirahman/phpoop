<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Car {
    private $brand;
    public $type;
    public $year;
    public $model;
    public $cc;
    
    public function __construct($brand, $type, $year, $model, $cc) {
	$this->brand = $brand;
	$this->type = $type;
	$this->year = $year;
	$this->model = $model;
	$this->cc = $cc;
    }
    
    public function showMe() {
	echo "From car class ".$this->brand;
    }
    
}

class BMW extends Car{
    
    
    protected $cost;
    
    public function __construct($brand, $type, $year, $model, $cc, $cost) {
	parent::__construct($brand, $type, $year, $model, $cc);
	$this->cost = $cost;
    }
    
    
    function getModel() {
	return $this->model;
    }
    
    public function setModel($model = "") {
	$this->model = $model;
    }
    
    public function showMe() {
	echo "From BMW class ".$this->brand;
	parent::showMe();
    }
    
    
    function __call($name, $arguments) {
	if($name == "showMilage") 
	    echo "i do not have $name";
    }
    
    
}

class X6 extends BMW {
    
    
    protected $cost;
    
    
    function getModel() {
	return $this->model;
    }
    
    public function setModel($model = "") {
	$this->model = $model;
    }
    
    public function showMe() {
	echo "From BMW class ".$this->brand;
	Car::showMe();
    }
    
}

$bmw1 = new BMW();

print_r($bmw1);


//$bmw1->brand = "BMW";
$bmw1->setModel("X3 2017");
echo $bmw1->showMe();

echo $bmw1->showMilage();