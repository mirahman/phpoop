<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class Square {
    
    public $side;
    
    public function __construct($side) {
	$this->side = $side;
    }
    
    public function area() {
	return $this->side * $this->side;
    }
    
}