<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Car {
    
    var $color;
    var $cc;
    var $model;
    var $maker;
    var $type;
    var $rimSize;
    static $className = "Car";
    
    function doBreak() {
	//global $className;
	echo "i am from ".$this->className."\n";
	echo "I am breaking\n";
	echo $this->cc;
    }
    
    static function accelerate() {
	return "I am speeding";
    }
}


$bmw = new Car;

print_r($bmw);

$bmw->cc = 2500;

$bmw->doBreak();

print_r($bmw);

$toyota = new Car();

$toyota->doBreak();
print_r($toyota);



echo "i am looking for ".Car::$className." and ".Car::accelerate();
