<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

interface geometry {
    public function area();
}
	
Class Rectangle implements geometry{
    
    public $width;
    public $height;
    
    public function __construct($width, $height) {
	$this->width = $width;
	$this->height = $height;
    }
    
    public function area() {
	return $this->height * $this->width;
    }
    
}

Class Sqaure implements geometry{
    
    public $side;
    
    public function __construct($side) {
	$this->side = $side;
    }
    
    public function area() {
	return $this->side * $this->side;
    }
    
}

Class Circle implements geometry{
    
    public $radius;
    
    public function __construct($radius) {
	$this->radius = $radius;
    }
    
    public function area() {
	return (22/7)*$this->radius * $this->radius;
    }
    
}

Class Calculate {
    
    public static function getArea(geometry $obj) {
	return $obj->area();
    }
    
}

$arr = [];
$arr[] = new Circle(10);
$arr[] = new Rectangle(20,15);
$arr[] = new Sqaure(12);


foreach($arr as $obj)
    echo Calculate::getArea($obj)."<br/>";