<?php
echo "<pre>";


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Ferrari {
    
    var $engine;
    var $color;
    
    const INTERESTRATE = 10;
    
    
    function __construct($engine, $color) {
	   // echo "I am constructing";
	$this->engine = $engine;
	$this->color = $color;
    }
    
    function __destruct() {
	echo "i am destructing <br />";
    }
    
    function goFast() {
	echo "my engine is ".$this->engine."<br />";
    }
    
    function __get($name) {
	echo "i do not have $name";
    }
    

    
    function __set($name, $value) {
	echo "i do not have $name, so cant assign $value";
    }
    
    function __call($name, $args) {
	echo "i do not have method $name";
    }
    
}

$f50 = new Ferrari("2500CC","red");

$f9 = new Ferrari("3600CC","blue");


$f50->goFast();


$f9->goFast();

 $f50->showMyName();

 $str = serialize($f9);
 
var_dump($f50);

$f9copy = unserialize($str);

var_dump($f9copy);
	
$f50 = NULL;

var_dump($f9);
